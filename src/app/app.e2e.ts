import { browser, by, element } from 'protractor';

describe('App', () => {

  beforeEach(() => {
    browser.get('/');
  });

  it('should have a title', () => {
    let subject = 'something';
    let result  = 'something';
    expect(subject).toEqual(result);
  });
});
